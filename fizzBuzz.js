const fizzBuzzCheck = count => {
  // console.log(count);
  return (newValue =
    count % 3 === 0 && count % 5 === 0
      ? 'fizzbuzz'
      : count % 3 === 0
      ? 'fizz'
      : count % 5 === 0
      ? 'buzz'
      : count);
};

const fizzBuzz = number => {
  // log out every number from one to 'num'
  let currentNum = 1;
  let newValue = null;
  const fizzBuzzArray = [];

  do {
    newValue = fizzBuzzCheck(currentNum);
    // console.log(currentNum, newValue);
    fizzBuzzArray.push(newValue);
    currentNum++;
  } while (currentNum <= number);
  // console.log(fizzBuzzArray);
  return fizzBuzzArray;
};
fizzBuzz(20);
module.exports = { fizzBuzz, fizzBuzzCheck };
