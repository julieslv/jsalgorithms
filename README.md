# JS Algorithms
Small node project to up skill my JS while adding the *Jasmine* test suites to the mix.

to install dependencies
`npm i` or `yarn`

to run the tests
`npm run test`
or
`yarn test`

use `node` [filename] to run the code.
