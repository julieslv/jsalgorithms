// Do we have enough words in out magazine to create a note?

// Returns true or false

const noteText = 'cats are great';
const magazineText =
  'Scientists have discovered narrowing your eyes is the best way to build a rapport with cats . cats are great';

const makeMagazineObj = magazineText => {
  const magazineArray = magazineText.split(' ');

  // using a hash table example
  const magazineObj = {};

  magazineArray.forEach(word => {
    if (!magazineObj[word]) magazineObj[word] = 0; // if the word is not present create it and set to 0
    magazineObj[word]++; // increment by one
  });
  return magazineObj;
};

const notePossible = (noteTextArray, magazineObj) => {
  let noteIsPossible = true; // set to true initially
  noteTextArray.forEach(word => {
    console.log(word);
    if (magazineObj[word]) {
      magazineObj[word]--; // decrement for the object word
      if (magazineObj[word] < 0) noteIsPossible = false;
    } else noteIsPossible = false;
  });
  return noteIsPossible;
};

const harmlessReporter = (noteText, magazineText) => {
  const magazineObj = makeMagazineObj(magazineText);
  const noteTextArray = noteText.split(' ');
  notePossible(noteTextArray, magazineObj);
};

harmlessReporter(noteText, magazineText);

module.exports = { notePossible, makeMagazineObj };
