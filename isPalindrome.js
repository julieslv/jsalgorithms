const isPalindrome = (string)=>{
  // return true or false
  string= string.toLowerCase();
  const charArray = string.split('')
  console.log(charArray);
  const validChar = 'abcdefghijklmnopqrstuvwxyz';

  const regex = /[a-z]/;

  // console.log(regex)

  let lettersArray = [];
  charArray.forEach(char => {
    if(regex.test(char)) lettersArray.push(char);
  });

  if(lettersArray.join('') === lettersArray.reverse().join(''))return true;


}

const test = isPalindrome("Madam I'm Adam");


module.exports = { isPalindrome };
