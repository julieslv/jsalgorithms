const {caesarCipher} = require('../caesarCipher');

describe('Caesar Cipher', () => {
  it('should return ccc when given aaa and 2', () => {
    expect(caesarCipher('aaa', 2)).toBe('ccc');
  });
  it('should return the correct letters in upperCase given aAa and 1', () => {
    expect(caesarCipher('aAa', 1)).toBe('bBb');
  });
  it('should return to the start of the alphabet with bbb when the end in reached given zzz and 2', () => {
    expect(caesarCipher('zzz', 2)).toBe('bbb');
  });
})

