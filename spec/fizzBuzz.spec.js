const { fizzBuzz, fizzBuzzCheck } = require('../fizzBuzz');

describe('FizzBuzz', () => {
  it('should return an array of values', function () {
    const expectedArray = [
      1,
      2,
      'fizz',
      4,
      'buzz',
      'fizz',
      7,
      8,
      'fizz',
      'buzz',
      11,
      'fizz',
      13,
      14,
      'fizzbuzz',
      16,
      17,
      'fizz',
      19,
      'buzz',
    ];
    const fizzBuzzArray = fizzBuzz(20);
    expect(fizzBuzzArray).toEqual(expectedArray);
  });

  describe('FizzBuzz check', () => {
    it('if the number is divisible by 3', () => {
      const fizzCheck = fizzBuzzCheck(3);
      expect(fizzCheck).toBe('fizz');
    });
    it('if the number is divisible by 5', () => {
      const buzzCheck = fizzBuzzCheck(5);
      expect(buzzCheck).toBe('buzz');
    });
    it('if the number is divisible by 5 and 3', () => {
      const fizzAndBuzzCheck = fizzBuzzCheck(15);
      expect(fizzAndBuzzCheck).toBe('fizzbuzz');
    });
  });
});
