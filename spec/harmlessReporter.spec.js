const { notePossible, makeMagazineObj } = require('../harmlessReporter');

describe('Harmless reporter', () => {
  describe('notePossible', () => {
    const noteTextArray = ['cats', 'are', 'great'];
    it('should return TRUE when all words are available', () => {
      const magazineObj = {
        Scientists: 1,
        have: 1,
        discovered: 1,
        narrowing: 1,
        your: 1,
        eyes: 1,
        is: 1,
        the: 1,
        best: 1,
        way: 1,
        to: 1,
        build: 1,
        a: 1,
        rapport: 1,
        with: 1,
        cats: 2,
        '.': 1,
        are: 1,
        great: 1,
      };

      const booleanNote = notePossible(noteTextArray, magazineObj);
      expect(booleanNote).toBeTruthy();
    });

    it('should return FALSE when all words are not available', () => {
      const magazineObj = {
        Scientists: 1,
        have: 1,
        discovered: 1,
        narrowing: 1,
        your: 1,
        eyes: 1,
        is: 1,
        the: 1,
        best: 1,
        way: 1,
        to: 1,
        build: 1,
        a: 1,
        rapport: 1,
      };

      const booleanNote = notePossible(noteTextArray, magazineObj);
      expect(booleanNote).toBeFalsy();
    });
  });

  describe('makeMagazineObj', () => {
    it('should return an object', () => {
      const magazineText =
        'Scientists have discovered narrowing your eyes is the best way to build a rapport with cats . cats are great';
      const magazineObj = makeMagazineObj(magazineText);
      const expectedObj = {
        Scientists: 1,
        have: 1,
        discovered: 1,
        narrowing: 1,
        your: 1,
        eyes: 1,
        is: 1,
        the: 1,
        best: 1,
        way: 1,
        to: 1,
        build: 1,
        a: 1,
        rapport: 1,
        with: 1,
        cats: 2,
        '.': 1,
        are: 1,
        great: 1,
      };
      expect(magazineObj).toEqual(expectedObj);
    });
  });
});
